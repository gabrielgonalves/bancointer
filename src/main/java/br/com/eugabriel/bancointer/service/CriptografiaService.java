package br.com.eugabriel.bancointer.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import br.com.eugabriel.bancointer.entity.Usuario;

@Service
public class CriptografiaService {

	private static final String ALGORITMO = "RSA";
	private static final String UTF8 = "UTF-8";

	Map<Usuario, Object> chaves = new HashMap<>();

	public void criarChave(Usuario usuario) {
		try {
			KeyPairGenerator gerador = KeyPairGenerator.getInstance(ALGORITMO);
			gerador.initialize(2048);

			KeyPair chave = gerador.generateKeyPair();

			chaves.put(usuario, chave);
		} catch (Exception e) {
		}
	}

	public boolean podeCriptografar(Usuario usuario) {
		return chaves.get(usuario) == null;
	}

	public String criptografar(String valor, PublicKey chave) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ALGORITMO);
		cipher.init(Cipher.ENCRYPT_MODE, chave);
		return Base64.encodeBase64String(cipher.doFinal(valor.getBytes(UTF8)));
	}

	public String descriptografar(String valor, PrivateKey chave) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ALGORITMO);
		cipher.init(Cipher.DECRYPT_MODE, chave);
		return new String(cipher.doFinal(Base64.decodeBase64(valor)), UTF8);
	}

	public Usuario criptografar(Usuario usuario) {
		PublicKey chave = ((KeyPair) chaves.get(usuario)).getPublic();

		if (chave != null) {
			try {
				usuario.setNome(criptografar(usuario.getNome(), chave));
				usuario.setEmail(criptografar(usuario.getEmail(), chave));
			} catch (Exception e) {

			}
		}

		return usuario;
	}

	public Usuario descriptografar(Usuario usuario) {
		PrivateKey chave = ((KeyPair) chaves.get(usuario)).getPrivate();

		if (chave != null) {
			try {
				usuario.setNome(descriptografar(usuario.getNome(), chave));
				usuario.setEmail(descriptografar(usuario.getEmail(), chave));
			} catch (Exception e) {

			}
		}

		chaves.remove(usuario);

		return usuario;
	}

}
