package br.com.eugabriel.bancointer.service;

import java.util.LinkedHashMap;

import org.springframework.stereotype.Service;

import br.com.eugabriel.bancointer.entity.DigitoUnico;

@Service
public class CacheService {
	
	private static final Integer TAMANHO_CACHE = 10;

	private LinkedHashMap<DigitoUnico, Integer> cache = new LinkedHashMap<>();
	
	public Integer verificarCache(DigitoUnico digitoUnico) {
		if(cache.containsKey(digitoUnico)) {
			return cache.get(digitoUnico);
		}
		return null;
	}
	
	public void adicionarCache(DigitoUnico digitoUnico) {
		if(cache.size() == TAMANHO_CACHE) {
			cache.remove(cache.keySet().stream().findFirst().orElse(null));
		}
		
		cache.put(digitoUnico, digitoUnico.getResultado());
	}
}
