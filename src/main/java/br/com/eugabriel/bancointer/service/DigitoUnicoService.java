package br.com.eugabriel.bancointer.service;

import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class DigitoUnicoService {
	
	public Integer digitoUnico(String n, Integer k) {
		return Integer.valueOf(calcularDigitoUnico(n.repeat(k)));
	}

	private String calcularDigitoUnico(String digitos) {
		if(digitos != null && digitos.length() > 1) {
			return calcularDigitoUnico(String.valueOf(digitos.chars().mapToObj(Character::getNumericValue).collect(Collectors.toList()).stream().mapToInt(Integer::intValue).sum()));
		}
		return digitos;
	}
	
}
