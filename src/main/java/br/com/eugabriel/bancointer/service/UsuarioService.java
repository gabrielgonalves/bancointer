package br.com.eugabriel.bancointer.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.eugabriel.bancointer.entity.Usuario;
import br.com.eugabriel.bancointer.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository repository;
	
	public Usuario findUsuarioById(Long id) {
		Optional<Usuario> result = repository.findById(id);
		Usuario usuario = result.get();
		return usuario;
	}
	
	
}
