package br.com.eugabriel.bancointer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eugabriel.bancointer.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
