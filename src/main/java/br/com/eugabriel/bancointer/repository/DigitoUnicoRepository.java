package br.com.eugabriel.bancointer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eugabriel.bancointer.entity.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

}
