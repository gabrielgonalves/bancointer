package br.com.eugabriel.bancointer.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(description = "Usuário")
public class Usuario implements Serializable {

	private static final long serialVersionUID = -728065793033315107L;

	@Id
	@GeneratedValue
	@ApiModelProperty(notes = "ID do usuário")
	private Long id;

	@NotBlank
	@Column(length = 500)
	@ApiModelProperty(notes = "Nome do usuário")
	private String nome;

	@NotBlank
	@Column(length = 500)
	@ApiModelProperty(notes = "E-mail do usuário")
	private String email;

	@OneToMany(cascade = CascadeType.ALL)
	@ApiModelProperty(notes = "Lista de dígitos unicos calculados pelo usuário")
	private List<DigitoUnico> listaDigitoUnico;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DigitoUnico> getListaDigitoUnico() {
		return listaDigitoUnico;
	}

	public void setListaDigitoUnico(List<DigitoUnico> listaDigitoUnico) {
		this.listaDigitoUnico = listaDigitoUnico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
