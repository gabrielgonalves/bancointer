package br.com.eugabriel.bancointer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.eugabriel.bancointer.entity.DigitoUnico;
import br.com.eugabriel.bancointer.entity.Usuario;
import br.com.eugabriel.bancointer.repository.UsuarioRepository;
import br.com.eugabriel.bancointer.service.CacheService;
import br.com.eugabriel.bancointer.service.DigitoUnicoService;
import br.com.eugabriel.bancointer.service.UsuarioService;

@RestController
@RequestMapping("/digitoUnico")
public class DigitoUnicoController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private DigitoUnicoService service;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private CacheService cacheService;

	@GetMapping
	public ResponseEntity<DigitoUnico> recuperarDigitoUnico(@RequestBody @Valid DigitoUnico digitoUnico) {

		if (digitoUnico == null) {
			return ResponseEntity.notFound().build();
		}

		Integer retornoCache = cacheService.verificarCache(digitoUnico);
		if (retornoCache != null) {
			digitoUnico.setResultado(retornoCache);
		} else {
			digitoUnico.setResultado(service.digitoUnico(digitoUnico.getN(), digitoUnico.getK()));
			cacheService.adicionarCache(digitoUnico);
		}

		return ResponseEntity.ok(digitoUnico);
	}

	@PutMapping("/usuario/{id}")
	public ResponseEntity<Usuario> calcularDigitoUnicoUsuario(@RequestBody @Valid DigitoUnico digitoUnico,
			@PathVariable Long id) {
		Usuario usuario = usuarioService.findUsuarioById(id);

		if (usuario == null) {
			ResponseEntity.notFound().build();
		}

		Integer retornoCache = cacheService.verificarCache(digitoUnico);
		if (retornoCache != null) {
			digitoUnico.setResultado(retornoCache);
		} else {
			digitoUnico.setResultado(service.digitoUnico(digitoUnico.getN(), digitoUnico.getK()));
			cacheService.adicionarCache(digitoUnico);
		}
		usuario.getListaDigitoUnico().add(digitoUnico);

		usuario = usuarioRepository.save(usuario);

		return ResponseEntity.ok(usuario);
	}

	@GetMapping("/usuario/{id}")
	public ResponseEntity<List<DigitoUnico>> recuperarListaDigitoUnicoUsuario(@PathVariable Long id) {
		Usuario usuario = usuarioService.findUsuarioById(id);

		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(usuario.getListaDigitoUnico());
	}

}
