package br.com.eugabriel.bancointer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.eugabriel.bancointer.entity.Usuario;
import br.com.eugabriel.bancointer.repository.UsuarioRepository;
import br.com.eugabriel.bancointer.service.CriptografiaService;
import br.com.eugabriel.bancointer.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Usuário")
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private UsuarioService service;
	
	@Autowired
	private CriptografiaService criptografiaService;

	@ApiOperation(value = "Criar novo usuário", response = Usuario.class)
	@PostMapping
	public ResponseEntity<Usuario> criar(@Valid @RequestBody Usuario usuario) {
		Usuario usuarioPersistido = repository.save(usuario);
		return ResponseEntity.ok(usuarioPersistido);
	}

	@ApiOperation(value = "Listar todos usuários", response = List.class)
	@GetMapping
	public ResponseEntity<List<Usuario>> recuperarTodos() {
		List<Usuario> usuarios = repository.findAll();
		return ResponseEntity.ok(usuarios);
	}

	@ApiOperation(value = "Listar um usuário", response = List.class)
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> recuperar(@PathVariable Long id) {
		Usuario usuario = service.findUsuarioById(id);

		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(usuario);
	}

	@ApiOperation(value = "Atualizar usuário", response = List.class)
	@PutMapping("/{id}")
	public ResponseEntity<Usuario> atualizar(@PathVariable Long id, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioRecuperado = service.findUsuarioById(id);

		if (usuarioRecuperado == null) {
			return ResponseEntity.notFound().build();
		}

		BeanUtils.copyProperties(usuario, usuarioRecuperado, "id");

		usuarioRecuperado = repository.save(usuarioRecuperado);

		return ResponseEntity.ok(usuarioRecuperado);
	}

	@ApiOperation(value = "Excluir usuário", response = List.class)
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> excluir(@PathVariable Long id) {
		Usuario usuario = service.findUsuarioById(id);

		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}

		repository.delete(usuario);

		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Criptografar usuário", response = List.class)
	@PutMapping("/{id}/criptografar")
	public ResponseEntity<Usuario> criptografarUsuario(@PathVariable Long id) {
		Usuario usuario = service.findUsuarioById(id);
		
		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}
		
		if(criptografiaService.podeCriptografar(usuario)) {
			criptografiaService.criarChave(usuario);
			usuario = criptografiaService.criptografar(usuario);
			
			repository.save(usuario);
			
			return ResponseEntity.ok(usuario);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
	@ApiOperation(value = "Descriptografar usuário", response = List.class)
	@PutMapping("/{id}/descriptografar")
	public ResponseEntity<Usuario> descriptografarUsuario(@PathVariable Long id) {
		Usuario usuario = service.findUsuarioById(id);
		
		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}
		
		if(!criptografiaService.podeCriptografar(usuario)) {
			usuario = criptografiaService.descriptografar(usuario);
			
			repository.save(usuario);
			
			return ResponseEntity.ok(usuario);
		}
		
		return ResponseEntity.badRequest().build();
	}

}
