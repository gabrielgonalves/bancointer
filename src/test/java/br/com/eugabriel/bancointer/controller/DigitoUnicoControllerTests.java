package br.com.eugabriel.bancointer.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eugabriel.bancointer.entity.DigitoUnico;
import br.com.eugabriel.bancointer.entity.Usuario;
import br.com.eugabriel.bancointer.repository.DigitoUnicoRepository;
import br.com.eugabriel.bancointer.repository.UsuarioRepository;
import br.com.eugabriel.bancointer.service.CacheService;
import br.com.eugabriel.bancointer.service.DigitoUnicoService;
import br.com.eugabriel.bancointer.service.UsuarioService;

@RunWith(SpringRunner.class)
@WebMvcTest(DigitoUnicoController.class)
public class DigitoUnicoControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DigitoUnicoRepository repository;

	@MockBean
	private UsuarioRepository usuarioRepository;

	@MockBean
	private DigitoUnicoService service;

	@MockBean
	private UsuarioService usuarioService;

	@MockBean
	private CacheService cacheService;

	private String jsonDigitoUnico = "{\"n\":\"9875\",\"k\":\"4\"}";
	private Usuario usuario = Mockito.mock(Usuario.class);
	private Long id = 1L;

	@Test
	public void recuperarDigitoUnico() throws Exception {
		when(cacheService.verificarCache(any())).thenReturn(8);
		
		mockMvc.perform(get("/digitoUnico").accept(MediaType.APPLICATION_JSON).content(jsonDigitoUnico)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultado", is(8)));
		
		when(cacheService.verificarCache(any())).thenReturn(null);
		when(service.digitoUnico(any(), any())).thenReturn(8);
		
		mockMvc.perform(get("/digitoUnico").accept(MediaType.APPLICATION_JSON).content(jsonDigitoUnico)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
		.andExpect(jsonPath("$.resultado", is(8)));
	}
	
	@Test
	public void recuperarDigitoUnicoUsuario() throws Exception {
		when(usuarioService.findUsuarioById(id)).thenReturn(usuario);
		when(usuario.getListaDigitoUnico()).thenReturn(Collections.singletonList(new DigitoUnico()));
		when(cacheService.verificarCache(any())).thenReturn(8);
		
		mockMvc.perform(get("/digitoUnico/usuario/"+id).accept(MediaType.APPLICATION_JSON).content(jsonDigitoUnico)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)));
		
		when(usuarioService.findUsuarioById(any())).thenReturn(null);
		
		mockMvc.perform(get("/digitoUnico/usuario/"+id).accept(MediaType.APPLICATION_JSON).content(jsonDigitoUnico)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}
}
