package br.com.eugabriel.bancointer.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eugabriel.bancointer.entity.Usuario;
import br.com.eugabriel.bancointer.repository.UsuarioRepository;
import br.com.eugabriel.bancointer.service.UsuarioService;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsuarioRepository repository;

	@MockBean
	private UsuarioService service;

	private Long id = 11L;
	private Usuario usuario = Mockito.mock(Usuario.class);
	private List<Usuario> usuarios = Collections.singletonList(usuario);
	private String jsonNovoUsuario = "{\"nome\":\"Gabriel\",\"email\":\"gabriel.ggfreitas@gmail.com\"}";
	private String jsonEditarUsuario = "{\"nome\":\"Gabriel Gonçalves\",\"email\":\"gabriel.ggfreitas@gmail.com\"}";

	@Test
	public void criar() throws Exception {
		mockMvc.perform(post("/usuario").accept(MediaType.APPLICATION_JSON).content(jsonNovoUsuario)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void recuperarTodos() throws Exception {
		when(repository.findAll()).thenReturn(usuarios);

		mockMvc.perform(get("/usuario").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void recuperar() throws Exception {
		when(service.findUsuarioById(id)).thenReturn(usuario);

		mockMvc.perform(get("/usuario/" + id).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

		when(service.findUsuarioById(id)).thenReturn(null);

		mockMvc.perform(put("/usuario/" + id).accept(MediaType.APPLICATION_JSON).content(jsonEditarUsuario)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void atualizar() throws Exception {
		when(service.findUsuarioById(id)).thenReturn(usuario);

		mockMvc.perform(put("/usuario/" + id).accept(MediaType.APPLICATION_JSON).content(jsonEditarUsuario)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

		when(service.findUsuarioById(id)).thenReturn(null);

		mockMvc.perform(put("/usuario/" + id).accept(MediaType.APPLICATION_JSON).content(jsonEditarUsuario)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void excluir() throws Exception {
		when(service.findUsuarioById(id)).thenReturn(usuario);

		mockMvc.perform(delete("/usuario/" + id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());

		when(service.findUsuarioById(id)).thenReturn(null);

		mockMvc.perform(delete("/usuario/" + id).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
}
